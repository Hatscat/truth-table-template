function truthTable(texts, ...values) {
    console.log("texts", texts);
    // console.log("values", values.length, values);
    // (n-1) + (1 << (n-1)); // 1 + 2 = 3; 2 + 4 = 6; 3 + 8 = 11; 4 + 16 = 20;
    const variablesCount = Math.floor(Math.log2(values.length));

    if (values.length !== variablesCount + (1 << variablesCount)) {
        throw new Error(`[TruthTable] Incorrect number of rows! (${values.length - variablesCount} instead of ${1 << variablesCount}).`);
    }

    const result = values
        .slice(0, variablesCount)
        .reduce((n, b, i, a) => n + ((b ? 1 : 0) << (a.length - i - 1)), 0);
    // console.log('>> result', result)
    return values[variablesCount + result];
}

const v1 = truthTable`
 ${1 < 2} |
     0    | ${2}
     1    | ${6}`;

console.log('v1', 'expect 6', v1);

const v2 = truthTable`
 ${1 < 2} | ${3 > 4} |
     _    |     _    | ${2}
     _    |    XXX   | ${4}
    XXX   |     _    | ${6}
    XXX   |    XXX   | ${8}`;

console.log('v2', 'expect 6', v2);

const v3 = truthTable`
 ${true} | ${false} | ${true} | Result
---------|----------|---------|------
         |          |         | ${1}
---------|----------|---------|------
         |          |XXXXXXXXX| ${2}
---------|----------|---------|------
         |XXXXXXXXXX|         | ${3}
---------|----------|---------|------
         |XXXXXXXXXX|XXXXXXXXX| ${4}
---------|----------|---------|------
XXXXXXXXX|          |         | ${5}
---------|----------|---------|------
XXXXXXXXX|          |XXXXXXXXX| ${6}
---------|----------|---------|------
XXXXXXXXX|XXXXXXXXXX|         | ${7}
---------|----------|---------|------
XXXXXXXXX|XXXXXXXXXX|XXXXXXXXX| ${8}
-------------------------------------`;

console.log('v3', 'expect 6', v3);

// const v2Error = truthTable`
//  ${1 < 2} | ${3 > 4} |
//      _    |     _    | ${2}
//      _    |    XXX   | ${4}
//     XXX   |     _    | ${6}`;

// TODO: test with too much rows too